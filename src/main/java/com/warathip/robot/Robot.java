package com.warathip.robot;

public class Robot extends Unit {

    public Robot(Map map, char symbol, int x, int y) {
        super(map, symbol, x, y, false);
    }
    public void up() {
        int y = this.gety();
        y--;
        this.sety(y);
    }
    public void down() {
        int y = this.gety();
        y++;
        this.sety(y);
    }
    public void left() {
        int x = this.getx();
        x--;
        this.setx(x);
    }
    public void right() {
        int x = this.getx();
        x++;
        this.setx(x);
    }
}
